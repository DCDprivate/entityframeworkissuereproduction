using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;

using System.Configuration;
using System.Collections.Generic;

namespace FunctionApp2
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            var testoutputs = new List<string>();

            //app.config 1
            testoutputs.Add(testconnectionstring("postgres-key-config"));

            //app.config 2
            testoutputs.Add(testconnectionstring("possible-stackoverflow-fix-config"));

            //local.settings.json 1
            testoutputs.Add(testconnectionstring("postgres-key-json"));

            //local.settings.json 2
            testoutputs.Add(testconnectionstring("possible-stackoverflow-fix-json"));

            return await Task.Run(() => {
                return req.CreateResponse(testoutputs);
            });
        }

        /// <summary>
        /// returns result or error message.
        /// </summary>
        /// <param name="connectionstringkey"></param>
        /// <returns></returns>
        public static string testconnectionstring(string connectionstringkey)
        {
            Model1 database = null;
            string firstlanguage = string.Empty;
            string output = $"{connectionstringkey}: ";

            try
            {
                database = new Model1(ConfigurationManager
                    .ConnectionStrings[connectionstringkey]
                    .ConnectionString);

                firstlanguage = database
                    .languages
                    .FirstOrDefault()
                    .language;
                return output + $"{firstlanguage}";
            }

            catch (System.Exception exception)
            {
                return output +  $"{exception.Message}";
            }

            finally
            {
                if (database != null)
                {
                    database.Dispose();
                }
            }
        }
    }
}
